# Introduction
As an Amateur Radio Volunteer Examiner (VE) and occasional instructor,
I see candidates and students wanting example tests.
There are sites on-line which present questions in a more modern on-line examination format.

Amateur Radio exams are still nominally given in "paper and pencil" format.
Exams produced by this application reflect the form and content of the actual exams used during VE sessions.
This application can generate valid exam booklets using the same question pools used by VEs.
The application adds appropriate warnings to not write in the exam booklet or, if answer patterns `ABCD` or `DCBA` are chosen, that the exam is not valid.

NOTE: This application does not attempt to produce the diagrams required by some questions.
The appropriate diagram page(s) need(s) to be manually included in the exam booklet based on the questions chosen for the particular exam.
PDF documents containing the diagrams are included in both the repository and common files.

Example exams are generated daily and [posted here](https://kronenpj.gitlab.io/ve_exam_generator/).

# Installation / Set up
The application does not need to be "installed" as such, but some set up is required to successfully generate exams. There are two paths to achieve proper set up.

1. Use the git repository directly:
   a. Clone the [git](https://gitlab.com/kronenpj/ve_exam_generator.git) repository.
   b. Create a Python virtual environment in the cloned directory. Run either `create_venv.sh` or `create_venv.bat` depending on your environment.
   c. Activate that new environment. Execute either `. venv/bin/activate` or `venv/Scripts/activate.bat`.
   d. Run the desired program, e.g.: `python gen_exam.py [options]`

2. Use a single-file executable:
   a. Create a directory to hold the application and files.
   b. Download the common files and desired application(s) from [Github packages](https://gitlab.com/kronenpj/ve_exam_generator/-/packages).
   c. Extract the common files.
   d. Assure the single-file application is executable (Linux only, `chmod +x {file}`).
   e. Run the desired program, e.g.: `python gen_exam.py [options]`

# Custom Templates
Custom templates can be created by starting with one of the existing templates
(`html.j2`, `latex.j2` or `text.j2`) and selecting one as the basis for your
template.

The `text` template is the most basic and produces a plain-ASCII file.
This file can be printed directly or imported into a word processing application for additional formatting.
The `html` template results in a single-page HTML document suitable for viewing in a web browser.
Both these templates are used when generating these [sample exams](https://kronenpj.gitlab.io/ve_exam_generator/).
The `latex` template produces typeset-quality output, easily converted to PDF for printing.
The `latex` template requires [familiarity](https://latex-project.org/help/) with the [LaTeX](https://en.wikipedia.org/wiki/LaTeX) typesetting language to customize and a LaTeX installation to compile into a printable format.
Suitable software is [available](https://latex-project.org/get/) for major operating systems.

Copy the desired template to a new file name, retaining the `.j2` extension.
The new template can be edited with almost any text editor.
Windows Notepad does not recognize UNIX-style files and will display the file
as a single line.
A more advanced editor like Wordpad, Notepad++, or any modern programming IDE will
work well.
The new template will also appear in the application's "help" display.

Template files use the [Jinja2](https://jinja.palletsprojects.com/en/3.0.x/templates/) syntax.

Jinja is configured to recognize these variables:
   | Jinja Variable | Template Text |
   | --- | --- |
   | block_start_string | `\BLOCK{` |
   | block_end_string | `}` |
   | variable_start_string | `\VAR{` |
   | variable_end_string | `}` |
   | comment_start_string | `\#{` |
   | comment_end_string | `}` |
   | line_statement_prefix | `%%` |
   | line_comment_prefix | `%#` |

Valid variables available for substitution are:
   | Variable name | Content |
   | --- | --- |
   | `title` | The question pool name |
   | `valid_exam_text` | Standard warning text, either "PLEASE DO NOT WRITE IN THIS BOOKLET" or "NOT A VALID AMATEUR RADIO EXAM" |
   | `exam_template` | The answer / distractor randomization style |
   | `exam_number` | The random number seed used to generate the exam |
   | `qdata` | A dictionary containing question number and question text |
   | `qid` | A dictionary containing question number and question ID |
   | `adata` | A dictionary containing answers and distractors, in order A, B, C, D |
   | `correct` | A list containing correct answers, in question order |

# Usage
The application accepts several arguments, all of which are optional:
```
usage: gen_exam.py [-h] [-d DATABASE] [-f OUTPUTFILE] [-j {html.j2,latex.j2,text.j2}]
                [-p {1,2,3,T,t,G,g,E,e}] [-q]
                [-r {NONE,RANDOM,ABCD,DCBA,none,random,abcd,dcba}]
                [-s SEED] [-v] [-V]

An amateur radio exam generator.

optional arguments:
  -h, --help            show this help message and exit
  -d DATABASE, --database DATABASE
                        Choose the source database file
  -f OUTPUTFILE, --outputfile OUTPUTFILE
                        Specify an output file name. Supply a single dash (-) to output the exam to
                        stdout. If this option is omitted, a filename will be generated.
  -j {html.j2,latex.j2,text.j2}, --jinjafile {html.j2,latex.j2,text.j2}
                        Choose the Jinja template file for the exam
  -p {1,2,3,T,t,G,g,E,e}, --pool {1,2,3,T,t,G,g,E,e}
                        Choose the question pool for the exam, based on the order in which it appears
                        or the first character of the p_name column.
  -q, --no-question-shuffle
                        Disable question shuffling
  -r {NONE,RANDOM,ABCD,DCBA,none,random,abcd,dcba}, --randtype {NONE,RANDOM,ABCD,DCBA,none,random,abcd,dcba}
                        Choose the response randomization algorithm
  -s SEED, --seed SEED  Specify a test number to re-create
  -v                    Turn on verbose debugging output
  -V                    View application version information and exit
```

If no arguments are given, the application will prompt for the question pool
and use defaults for remaining options.

The `pool` option selects the question pool from those available in the 'ExamData' database.
The pool designations are:

| Pool                | Numeric | Upper | Lower |
| ------------------- | ------- | ----- | ----- |
| Technician Class    | 1       | T     | t     |
| General Class       | 2       | G     | g     |
| Amateur Extra Class | 3       | E     | e     |

**Note**: The numeric designation above **does not** correspond to the FCC element assignments to the exams.

The `randtype` option selects the randomization method for the question response choices. The options are:

| Random Type | Description                                                                                           |
| ----------- | ----------------------------------------------------------------------------------------------------- |
| none        | Use the pool order for the answers and distractors for each question.  *(Default)*                    |
| abcd, ABCD  | Change the correct answer to follow a pattern of A, B, C, D, A... and randomize the distractor order. |
| dcba, DCBA  | Change the correct answer to follow a pattern of D, C, B, A, D... and randomize the distractor order. |
| random      | Randomize the answers and distractors for each question.                                              |

An answer sheet with the correct answers appears after the exam questions.

The `jinjafile` option selects the Jinja2 template to use when preparing the exam and answer key. The supplied templates are:

| Output Type | Description                                                     |
| ----------- | --------------------------------------------------------------- |
| `html.j2`   | A HTML-formatted exam and similarly formatted list of answers.  |
| `latex.j2`  | A LaTeX formatted exam and similarly formatted list of answers. |
| `text.j2`   | A flat, plaintext exam and simple list of answers. *(Default)*  |

The three output types above, `html.j2`, `latex.j2`, and `text.j2` can be retrieved from the source repository or the `required_files` ZIP or TAR file.

User-defined templates, with a `.j2` extension, can be placed in the directory where the `gen_exam` executable exists. All `.j2` files will automatically be detected and available to use as `jinjafile` templates.

The `outputfile` option specifies a file to be written after generating the exam, including answers.
The default is to generate the exam to standard out. *(Default: `{PoolName}-{randomization}-{seed}.{output-type}`)*

The `seed` option allows a specific random seed to be provided to produce a duplicate exam.
This option takes an integer number. *(Default: generate random seed)*

The `no-question-shuffle` option disables question shuffling, causing the exam questions to be ordered as they are in the question pool. *(Default: shuffle questions)*

# Data Source
All the source data used by this application is generated by the National Conference of Volunteer Examiner Coordinators (NCVEC) and can be found on their website: https://ncvec.org/. The format of the database is taken from the now-deprecated ARRL exam generator. The text version of the question pools was generated by using LibreOffice to open each `docx` file and output to plain text using the command line: `libreoffice --convert-to "txt:Text:ASCII" source.docx`. The resulting `txt` file has lingering Microsoft Word artifacts which cause errors when parsing the file. These need to be manually converted to plain text characters such as `"`, `-` `'` and potentially others.
