#!/usr/bin/env python3
"""
Compare pool text files to supplied database.
"""
import argparse
import logging

from Levenshtein.StringMatcher import StringMatcher

import examgenerator.convert_parse_pool as cpp
from examgenerator.data import Data

mylog = logging.Logger(__name__)
# mylog.setLevel(logging.DEBUG)


def arg_parser():
    argp = argparse.ArgumentParser(
        description="Compares provided files to the provided database."
    )
    argp.add_argument("-d", "--database", type=str, default=None, required=True)
    argp.add_argument(
        "-l", "--lev", type=int, default=1, help="Levenshtein distance, default=1"
    )
    argp.add_argument("inputfile", nargs="*", type=str, default=None)

    return argp.parse_args()


def compare_questions():
    args = arg_parser()

    for source in args.inputfile:
        print(f"Checking {source} against database.")
        cpp.ParsePool.consume_pool(source)

        lev_distance = args.lev

        # subels, subsubels, qs, q_data
        (__, __, __, q_data) = cpp.ParsePool.collect_data_from_source()
        data = Data(args.database)

        # Compare with the original database.
        for question, value in q_data.items():
            try:
                mylog.debug("question: %s", value)
                database = data.question_data(question)

                lev_check(
                    question, value.q_text, database["text"], "text", lev_distance
                )
                check(question, value.q_answer, database["ans"], "correct answer")
                lev_check(
                    question, value.q_a, database["a"], "response A", lev_distance
                )
                lev_check(
                    question, value.q_b, database["b"], "response B", lev_distance
                )
                lev_check(
                    question, value.q_c, database["c"], "response C", lev_distance
                )
                lev_check(
                    question, value.q_d, database["d"], "response D", lev_distance
                )
            except IndexError:
                mylog.error("Question '%s' does not exist in the database.\n", question)


def check(question, pool_data, database, reference):
    if pool_data != database:
        mylog.error(
            "Difference: Question %s, %s does not match the database ",
            question,
            reference,
        )
        mylog.error("           Pool: %s", pool_data)
        mylog.error("       Database: %s\n", database)


def lev_check(question, pool_data, database, reference, lev_distance):
    lev = StringMatcher()

    lev.set_seqs(pool_data, database)
    if lev.distance() > lev_distance:
        mylog.error(
            "Difference: Question %s, %s does not match the database (distance: %d).",
            question,
            reference,
            lev.distance(),
        )
        mylog.error("           Pool: %s", pool_data)
        mylog.error("       Database: %s\n", database)


if __name__ == "__main__":  # pragma: no mutate
    # execute only if run as a script
    compare_questions()
