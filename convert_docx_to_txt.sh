#!/usr/bin/env bash

INPUT="/home/kronenpj/projects/LaurelVE/2022-2026 Technician Pool Release March 7 2022.docx"
OUTPUT="2022-2026 Technician Pool Release March 7 2022.txt"
DESIRED="2022-2026-Tech-Pool.txt"

libreoffice --convert-to "txt:Text:ASCII" "$INPUT"
mv "$OUTPUT" "$DESIRED"

INPUT="/home/kronenpj/projects/LaurelVE/2023-2027 General Pool Release April 15 2023.docx"
OUTPUT="2023-2027 General Pool Release April 15 2023.txt"
DESIRED="2023-2027-General-Pool.txt"

libreoffice --convert-to "txt:Text:ASCII" "$INPUT"
mv "$OUTPUT" "$DESIRED"

INPUT="/home/kronenpj/projects/LaurelVE/2024ExtraClassPoolwitherrataJan312024.docx"
OUTPUT="2024ExtraClassPoolwitherrataJan312024.txt"
DESIRED="2024-2028-Extra-Pool.txt"

libreoffice --convert-to "txt:Text:ASCII" "$INPUT"
mv "$OUTPUT" "$DESIRED"

