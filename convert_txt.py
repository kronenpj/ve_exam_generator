#!/usr/bin/env python3
"""
Convert pool text files application.
"""

from examgenerator import convert_txt_to_db as cttd

if __name__ == "__main__":  # pragma: no mutate
    # execute only if run as a script
    cttd.do_conversion()
