#!/usr/bin/env bash

if [ -z "$VIRTUAL_ENV" ]; then
  . venv/bin/activate
fi

OPTIONS="$@"
if [ -z "$OPTIONS" ]; then
  AFTER="tests/"
else
  AFTER=""
fi

coverage run --rcfile=pyproject.toml -m pytest -m "not hypothesis" --runslow "$@" $AFTER
coverage combine --rcfile=pyproject.toml
coverage xml --rcfile=pyproject.toml
