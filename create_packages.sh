#!/usr/bin/env bash

mv .venv .venv-off-pyinstaller
if [ -x /usr/bin/podman ]; then
  ENGINE=${ENGINE:-podman}
else
  ENGINE=${ENGINE:-docker}
fi
#DEBUG_OPTS="-d all"
REG_OPTS="-p src -F -c"
OPTIMIZE="PYTHONOPTIMIZE=2"
#CTR_WIN="cdrx/pyinstaller-windows"
#CTR_LIN="cdrx/pyinstaller-linux"
CTR_WIN="pyinstaller-win64:5.13"
CTR_LIN="pyinstaller-linux:5.13"
CTR_OPTS="--privileged --rm"

function build() {
  #  echo "Container image: $1"
  #  echo "Script to convert: $2"
  SCRIPT=${2%.py}
  case "${1}" in
  "${CTR_LIN}")
    [ -e dist/${SCRIPT} ] && mv -f dist/${SCRIPT} dist/${SCRIPT}-prev ;
    N_REG_OPTS="$REG_OPTS -s"
    ;;
  "${CTR_WIN}")
    [ -e dist/${SCRIPT}.exe ] && mv -f dist/${SCRIPT}.exe dist/${SCRIPT}-prev.exe ;
    N_REG_OPTS="$REG_OPTS"
    ;;
  esac

  capture=`${ENGINE} run ${CTR_OPTS} -v "$(pwd):/src/" ${1} \
          "${OPTIMIZE} pyinstaller ${N_REG_OPTS} ${DEBUG_OPTS} ${SCRIPT}.py" 2>&1`

  if [ $? != 0 ]; then
    echo "Build was not successful, rolling back."
    case "${1}" in
    "${CTR_LIN}")
      [ -e dist/${SCRIPT}-prev ] && mv -f dist/${SCRIPT}-prev dist/${SCRIPT};;
    "${CTR_WIN}")
      [ -e dist/${SCRIPT}-prev.exe ] && mv -f dist/${SCRIPT}-prev.exe dist/${SCRIPT}.exe;;
    esac
    echo "Process output:"
    echo $capture
  else
    echo "Build of ${SCRIPT} successful on ${1}"
  fi
}

poetry export -o requirements.txt --without-hashes
if [ -z "$1" ]; then
  for ctr in $CTR_LIN $CTR_WIN; do
    #${ENGINE} pull $ctr
    for scr in compare_db convert_txt gen_exam; do
      build $ctr $scr
    done
  done
  mv -f dist/gen_exam.exe dist/gen_exam-full.exe
  mv -f dist/gen_exam dist/gen_exam-full

  mv src/examtemplates src/.examtemplates
  build ${CTR_LIN} gen_exam
  build ${CTR_WIN} gen_exam
  mv src/.examtemplates src/examtemplates
else
  for ctr in $CTR_LIN $CTR_WIN; do
    #${ENGINE} pull $ctr
    build $ctr $1
  done
fi

rm -f requirements.txt
mv .venv-off-pyinstaller .venv
