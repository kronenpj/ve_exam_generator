#!/usr/bin/env bash

PY_BIN=${1:-python3.10}

if [ -x /usr/bin/${PY_BIN} ];then
  PY=${PY_BIN}
else
  PY=python3
fi

/usr/bin/${PY} -m venv .venv --clear --prompt VEexamgen
source .venv/bin/activate
pip install poetry
#pip install -r requirements.txt -r requirements-test.txt
poetry install --no-root
