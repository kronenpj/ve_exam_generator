#!/usr/bin/env python3
"""
Main exam generator application.
"""

from examgenerator import application

if __name__ == "__main__":  # pragma: no mutate
    # execute only if run as a script
    application()
