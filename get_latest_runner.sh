#!/usr/bin/env bash

mv gitlab-runner-linux-amd64 gitlab-runner-linux-amd64-prev

wget -c https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

chmod +x gitlab-runner-linux-amd64
