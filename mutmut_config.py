""" Adjust mutmut's behavior."""

# pylint: disable=missing-function-docstring


def pre_mutation(context):
    skip_source = [
        "src/examgenerator/custom_log.py",
        "src/examgenerator/convert_txt_to_db.py",
    ]
    strings = [
        "_get_input",
        "argp.add_argument",
        "argparse.ArgumentParser",
        "assert",
        "help=",
        "if __name__ ==",
        "log",
        "mylog",
        "outstream.write",
        "print",
        "re.compile",
        "sys.exit",
    ]
    if context.filename in skip_source:
        context.skip = True
        return
    for string in strings:
        if string in context.current_source_line:
            context.skip = True
            break
