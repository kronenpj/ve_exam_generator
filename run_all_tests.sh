#!/usr/bin/env bash

if [ -z "$VIRTUAL_ENV" ]; then
  . venv/bin/activate
fi

tox -p --notest -c pyproject.toml "$@"

if [ "$1" == "" ]; then
  tox -p -c pyproject.toml -- -m 'not hypothesis'
else
  tox -p -c pyproject.toml "$@"
fi

# Exit if the test failed.
if [ $? == 1 ]; then
    exit $?
fi

if [ -d examtemplates ]; then
    mv examtemplates .examtemplates
    if [ "$1" == "" ]; then
      tox -p -c pyproject.toml -- -m 'not hypothesis'
    else
      tox -p -c pyproject.toml "$@"
    fi
    RETCODE=$?
    mv .examtemplates examtemplates
fi
exit $RETCODE
