#!/usr/bin/env bash

RUNNER_EXECUTOR="podman"
RUNNER_EXECUTOR="docker"
S3_HOST=server.n2kiq.home.arpa:9393
S3_BUCKET=gitlab-veexamgen
BUILDS_DIR=/tmp/ve_generator_build
ACCESS_KEY=$(grep aws_access_key_id ~/.aws/credentials | awk '{print $3}')
ACCESS_SECRET=$(grep aws_secret_access_key ~/.aws/credentials | awk '{print $3}')
OPTIONS="--docker-privileged --docker-pull-policy if-not-present"

if [ -d /dev/vboxusb ]; then
  echo "******************************************************************"
  echo "WARNING!!! /dev/vboxusb exists and has caused problems with"
  echo "           podman in the past. Please consider running:"
  echo "           rm -rf /dev/vboxusb"
  echo "           if an error occurs below during environment preparation"
  echo "******************************************************************"
fi

#if [ -z "$XDG_RUNTIME_DIR" ]; then
#  PODMAN_SOCKET=/run/user/$(id --user)/podman/podman.sock
#else
#  PODMAN_SOCKET=$XDG_RUNTIME_DIR/podman/podman.sock
#fi

if [ ! -s $PODMAN_SOCKET ]; then
  systemctl --user start podman.socket
fi

if [ ! -z "$ACCESS_KEY" ]; then
  S3_ACCESS="--cache-type s3 --cache-s3-server-address ${S3_HOST}
  --cache-s3-access-key=${ACCESS_KEY} --cache-s3-secret-key=${ACCESS_SECRET}
  --cache-s3-bucket-name=gitlab-veexamgen --cache-s3-insecure"
fi

#mkdir $BUILDS_DIR
#./gitlab-runner-linux-amd64 exec shell test --builds-dir=$BUILDS_DIR
#rm -rf $BUILDS_DIR

AFTER=""
if [ -z "$@" ]; then
  AFTER=test
fi

#./gitlab-runner-linux-amd64 exec docker --docker-privileged ${S3_ACCESS} "$@" test
#DOCKER_HOST=unix://$PODMAN_SOCKET ./gitlab-runner-linux-amd64 exec docker ${OPTIONS} ${S3_ACCESS} "$@" ${AFTER}
./gitlab-runner-linux-amd64 exec docker ${OPTIONS} ${S3_ACCESS} "$@" ${AFTER}
