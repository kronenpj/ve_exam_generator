#!/usr/bin/env bash

PYVER=${PYVER:-py310}

if [ -z "$VIRTUAL_ENV" ]; then
  . venv/bin/activate
fi

tox -c pyproject.toml -e ${PYVER} --notest "$@"

if [ "$1" == "" ]; then
  tox -c pyproject.toml -e ${PYVER} -- -m 'not hypothesis'
else
  tox -c pyproject.toml -e ${PYVER} "$@"
fi

exit $?
