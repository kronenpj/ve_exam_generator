#!/usr/bin/env bash

DEBUG=${DEBUG:-}

if [ -z "$VIRTUAL_ENV" ]; then
  . venv/bin/activate
fi

if [ -z "$@" ]; then
  $DEBUG mutmut run
else
  SRC=$(/bin/ls -1 src/examgenerator/*.py)
  for index in $SRC
  do
    $DEBUG mutmut run $index
  done
fi
