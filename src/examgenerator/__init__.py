"""
An amateur radio exam generator application.
"""

import contextlib

from .app import application
from .convert_parse_pool import ParsePool
from .convert_txt_to_db import do_conversion

with contextlib.suppress(ImportError):
    from examtemplates import *
