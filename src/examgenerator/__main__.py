"""
Run the application when invoked as a module.
"""
from . import application

if __name__ == "__main__":  # pragma: no mutate
    # execute only if run as a script
    application()
