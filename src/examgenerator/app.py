"""
Main exam generator application.
"""
import sys
from random import shuffle
from typing import Dict

from . import custom_log
from .arg_handler import arg_parser, handle_args
from .compose_exam import compose_exam_list
from .constants import EXAM_POOLS, EXAMDB_FILE
from .data import Data
from .output_exam import output_exam_jinja2
from .pool_choice import determine_pool

mylog = custom_log.get_logger()


def application():
    """
    The application itself
    """

    # Parse the command-line arguments, if any.
    args = arg_parser()
    # Deal with some of the command-line arguments
    rand_generator, randseed = handle_args(args)

    # Open the database and gather information on available question pools.
    try:
        data = Data()
        data.set_database_file(args.database)
    except (KeyError, AttributeError) as exception:
        # This time we need to exit because the database is missing.
        sys.stderr.write(
            "The database is not present or does not contain the correct structure.\n"
        )  # pragma: no mutate
        if args.database is not None:
            sys.stderr.write(
                f"Database {args.database} is corrupt, absent, empty or"
                "does not contain the correct structure.\n"
            )  # pragma: no mutate
        else:
            sys.stderr.write(
                f"Database {EXAMDB_FILE} is corrupt, absent, empty or"
                "not contain the correct structure.\n"
            )  # pragma: no mutate
        sys.stderr.write(f"Exception: {exception}.\n")
        sys.exit(1)
    pools: Dict[int, str] = data.pools()

    # Parse command line arguments / prompt user for the desired pool to use.
    use_pool = determine_pool(args, pools)

    # Acknowledge the choice the user asked for.
    # pragma: no mutate
    article = "an" if pools[use_pool][0] in ["A", "E", "I", "O", "U"] else "a"
    sys.stderr.write(
        f"\nYou chose to generate {article} {pools[use_pool]} exam.\n"
    )  # pragma: no mutate
    sys.stderr.write(f"Using template {args.randtype.upper()}.\n")  # pragma: no mutate
    sys.stderr.write(f"This is exam {randseed}.\n\n")  # pragma: no mutate

    questions = compose_exam_list(use_pool)

    # Shuffle questions if enabled
    if args.shufflequestions:
        mylog.debug(f"Shuffling questions. ({args.shufflequestions})")
        shuffle(questions)

    # Generate the output filename if none is supplied.
    if args.outputfile is None:
        args.outputfile = f"{EXAM_POOLS[use_pool]}-{args.randtype}-{randseed}.{args.jinja_file.replace('.j2','')}"  # pragma: no mutate

    # Normally the below will close sys.stdout when an output file is not chosen.
    # A semi-ugly workaround for this, if desired, is to override the close method:
    # (from:
    # https://stackoverflow.com/questions/17602878/how-to-handle-both-with-open-and-sys-stdout-nicely)
    if args.outputfile == "-":
        sys.stdout.close = lambda: None

    with (
        open(args.outputfile, "w") if args.outputfile != "-" else sys.stdout
    ) as outfile:
        assert outfile is not None
        output_exam_jinja2(
            outstream=outfile,
            pool=use_pool,
            questions=questions,
            examnumber=randseed,
            generator=rand_generator,
            template=args.randtype.upper(),
            jinja_file=args.jinja_file,
        )
