"""
Handle arguments for the application.
"""

import argparse
import logging
import os
import sys
from random import choice, randint, seed
from typing import Dict, Tuple

from sqlalchemy import exc

from . import custom_log
from .constants import EXAM_POOLS, EXAMDB_FILE, MAX_RANDOM
from .data import Data
from .globals import RAND_TYPES

mylog = custom_log.get_logger()


def arg_parser() -> argparse.Namespace:
    """
    Populate the arguments accepted by the application.

    :return: The customized parser output
    :rtype: argparse.Namespace
    """

    mylog.debug(f"Number of arguments: {len(sys.argv)}")

    try:
        data = Data()
        # This is will prevent creation of an empty EXAMDB_FILE.
        # Cause the call to data.pools to fail if the EXAMDB_FILE doesn't already exist.
        if os.path.exists(EXAMDB_FILE):
            data.set_database_file(EXAMDB_FILE)
        pools: Dict[int, str] = data.pools()
        data.dbsession.close()
    except (
        KeyError,
        AttributeError,
        exc.InvalidRequestError,
        exc.ArgumentError,
    ):  # pragma: no cover
        # This happens when the database file is missing or the contents are incorrect.
        # Populate with the values usually found in the database.
        # FIXME: If, for some reason, the database supplied by -d has different pools,
        # argparse will not let you choose one of them.
        # FIXME: Maybe this was a good idea, but is too 'cute.'
        pools = EXAM_POOLS
    finally:
        Data.engine = None

    argp = argparse.ArgumentParser(description="An amateur radio exam generator.")

    argp.add_argument(
        "-d",
        "--database",
        dest="database",
        type=str,
        default=EXAMDB_FILE,
        help="Choose the source database file",
    )
    argp.add_argument(
        "-f",
        "--outputfile",
        dest="outputfile",
        type=str,
        default=None,
        help="Specify an output file name. Supply a single dash (-) to "
        "output the exam to stdout. If this option is omitted, a filename will be generated.",
    )
    # Populate the jinja choice list from the existing files
    jinja_files = [f for f in os.listdir(".") if ".j2" in f]
    argp.add_argument(
        "-j",
        "--jinjafile",
        dest="jinja_file",
        choices=jinja_files,
        default="text.j2",
        help="Choose the Jinja template file for the exam",
    )
    # Populate the choice lists
    pool_opt_list = [str(index) for index in list(pools.keys())]
    for values in list(pools.values()):
        pool_opt_list.append(values[0])
        pool_opt_list.append(values[0].lower())
    argp.add_argument(
        "-p",
        "--pool",
        dest="pool",
        choices=pool_opt_list,
        default=-1,
        help="Choose the question pool for the exam, based on the order "
        "in which it appears or the first character of the p_name column.",
    )  # pragma: no mutate
    argp.add_argument(
        "-q",
        "--no-question-shuffle",
        dest="shufflequestions",
        default=True,
        action="store_false",
        help="Disable question shuffling",
    )
    # Populate the random types available
    random_types = list(RAND_TYPES)
    random_types.extend([x.lower() for x in RAND_TYPES if "TEMP" not in x])
    argp.add_argument(
        "-r",
        "--randtype",
        dest="randtype",
        choices=list(random_types),
        default="none",
        help="Choose the response randomization algorithm",
    )
    argp.add_argument(
        "-s",
        "--seed",
        dest="seed",
        type=int,
        default=0,
        help="Specify a test number to re-create",
    )  # pragma: no mutate
    argp.add_argument(
        "-v",
        dest="debug",
        action="store_true",
        help="Turn on verbose debugging output",
    )
    argp.add_argument(
        "-V",
        dest="version",
        action="store_true",
        help="View application version information and exit",
    )
    return argp.parse_args()


def handle_args(args: argparse.Namespace) -> Tuple[object, int]:
    """
    Handle some of the actions as appropriate given the arguments supplied to the application.

    :param args: Supplied arguments
    :type args: dict
    """

    # Handle the version flag.
    try:
        mylog.debug("Version argument: %s is %s", args.version, type(args.version))
        if args.version:  # pragma: no cover
            _version = "Not packaged."
            if os.path.exists("pyproject.toml"):
                with open("pyproject.toml") as toml:
                    for _tmp in toml:
                        if "version" in _tmp:
                            _version = _tmp.replace("version =", "").strip().strip('"')
                            break

            print("An amateur radio exam generator.")
            print(f"Version: {_version}")
            sys.exit()
    except ValueError:  # pragma: no cover
        mylog.debug("Version argument is not valid")
        # But ignore it.

    # Handle the database flag.
    try:
        mylog.debug("Database argument: %s is %s", args.database, type(args.database))
        if args.database is not None and not os.path.exists(args.database):
            print(f"Database {args.database} is not an existing file.")
            print(
                "Try creating one using a text version of the NCVEC pool and"
                "the convert_txt utility."
            )
            print("Exiting.")
            sys.exit()
    except ValueError:  # pragma: no cover
        print("Database argument is not valid")
        sys.exit(1)

    # Handle the debugging flag.
    try:
        mylog.debug("Debug argument: %s is %s", args.debug, type(args.debug))
        if args.debug:
            logging.getLogger(__package__).setLevel(logging.DEBUG)
            mylog.debug("Debugging enabled.")
    except ValueError:  # pragma: no cover
        mylog.debug("Debug argument is not valid")
        # But ignore it.

    # Handle the random seed, using the command-line argument if provided.
    randseed = randint(1, MAX_RANDOM)  # pragma: no mutate
    try:
        mylog.debug("Seed argument: %s is %s", args.seed, type(args.seed))
        if args.seed != 0:
            randseed = int(args.seed)
    except ValueError:  # pragma: no cover
        mylog.debug("Seed argument is not valid")
        # But ignore it.
    seed(randseed)

    # Handle the response randomization type, using the command-line argument if provided.
    rand_generator: object = RAND_TYPES["NONE"]
    try:
        if args.randtype.upper() == "TEMPRAND":
            most_types = [
                str(x) for x in list(RAND_TYPES.keys()) if x.startswith("TEMP")
            ]
            most_types.remove("TEMPRAND")
            args.randtype = choice(most_types)

        rand_generator = RAND_TYPES[args.randtype.upper()]
    except ValueError:  # pragma: no cover
        mylog.debug("Randomization type is not valid")
        # But ignore it.
    return rand_generator, randseed
