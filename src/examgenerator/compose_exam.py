"""
Choose a random set of questions from the provided pool.
"""
from random import choice

from . import custom_log
from .data import Data

mylog = custom_log.get_logger()


def compose_exam_list(pool: int) -> list:
    """Compose an exam from the selected pool."""

    data = Data()
    leadstrings = data.specs_pool_leadstrings(pool)
    lockedout = data.lockedout_questions(pool)

    mylog.debug("These questions are locked-out: %r", lockedout)
    question_list = []

    for prefix in leadstrings:
        mylog.debug("Gathering leadstrings for %s", prefix)
        group = data.questions_leadstring(prefix)

        # Handle locked out questions.
        for locked in lockedout:
            if locked in group:
                mylog.debug("Identified a locked-out question in group. Removing...")
                group.remove(locked)

        mylog.debug("Remaining leadstrings: %r", group)
        question_list.append(_choose_random(group))

    mylog.debug("Chosen questions: %r", ", ".join(question_list))
    return question_list


def _choose_random(collection: list) -> str:
    """Chooses an item from collection and returns the associated value."""

    return choice(collection)
