"""
Various constants used in the application.
"""

# Specify the highest-numbered seed when none is provided.
MAX_RANDOM = (
    3429289783  # pragma: no mutate  # 567582796136208883195862532581618194841600000
)

EXAMDB_FILE = "ExamData.db"

EXAM_POOLS = {
    1: "Technician",
    2: "General",
    3: "Extra",
}
# Reverse EXAM_POOLS and only take the first character of the value.
EXAM_INDICES = {v[0]: k for k, v in EXAM_POOLS.items()}

# Yes, this is probably done in a non-pythonic way.
ANS_LIST_CONVERSION = {
    "A": 0,
    "B": 1,
    "C": 2,
    "D": 3,
}
# Reverse ans_list_conversion
LIST_ANS_CONVERSION = {v: k for k, v in ANS_LIST_CONVERSION.items()}
