"""
Database Schema Declaration
"""

from sqlalchemy import Boolean, CheckConstraint, Column, Integer, String
from sqlalchemy.orm import DeclarativeBase

from .globals import Question


POOL_CONTENTS = {
    1: "1|NCVEC|el2.22|Technician|Technician Class Written Examination - Element 2"
    "|0|2022-07-01|2026-06-30|hamquestion|35|26|0|1|",
    2: "2|NCVEC|el3.23|General|General Class Written Examination - Element 3"
    "|1|2023-07-01|2027-06-30|hamquestion|35|26|0|1|",
    3: "3|NCVEC|el4.24|Extra|Extra Class Written Examination - Element 4"
    "|1|2024-07-01|2028-06-30|hamquestion|50|37|0|1|",
}


# Database structure
class Base(DeclarativeBase):
    pass


# CREATE TABLE hamquestion (q_p_id INTEGER, q_id TEXT, q_text TEXT, q_type INTEGER,
# q_ans TEXT, q_a TEXT, q_b TEXT, q_c TEXT,q_d TEXT, PRIMARY KEY (q_p_id,q_id));


class Hamquestion(Base):
    """
    Data class for the questions, distractors and other attributes.

    :param Base: ORM base class for database-derived classes.
    :type Base: sqlalchemy.ext.declarative.declarative_base
    """

    __tablename__ = "hamquestion"

    q_p_id = Column(Integer, nullable=False)
    q_id = Column(String, primary_key=True, nullable=False, default="BAD")
    q_text = Column(String)
    q_type = Column(String)
    q_ans = Column(String)
    q_a = Column(String)
    q_b = Column(String)
    q_c = Column(String)
    q_d = Column(String)

    def __init__(
        self,
        q_p_id=None,
        q_id=None,
        q_text=None,
        q_type=None,
        q_ans=None,
        q_a=None,
        q_b=None,
        q_c=None,
        q_d=None,
    ):
        self.q_p_id = q_p_id
        self.q_id = q_id
        self.q_text = q_text
        self.q_type = q_type
        self.q_ans = q_ans
        self.q_a = q_a
        self.q_b = q_b
        self.q_c = q_c
        self.q_d = q_d

    def __repr__(self):  # pragma: no cover
        return (
            f"<Hamquestion(q_p_id='{self.q_p_id}', q_id='{self.q_id}', "
            f"q_text='{self.q_text}', q_type='{self.q_type}', q_ans='{self.q_ans}', "
            f"q_a='{self.q_a}', q_b='{self.q_b}', q_c='{self.q_c}', q_d='{self.q_d}')>"
        )

    @staticmethod
    def add_question(question: Question):
        """
        Add supplied question to the database.

        :param question: Data for a specific question.
        :type question: Question
        :return: Database reference to the added data.
        :rtype: Hamquestion
        """
        return Hamquestion(
            q_p_id=question.q_p_id,
            q_id=question.q_id,
            q_text=question.q_text,
            q_type=None,
            q_ans=question.q_answer,
            q_a=question.q_a,
            q_b=question.q_b,
            q_c=question.q_c,
            q_d=question.q_d,
        )


# CREATE TABLE lockedout (lo_p_id INTEGER, lo_q_id TEXT, lo_canchange
# BOOLEAN, PRIMARY KEY (lo_p_id,lo_q_id));


class Lockedout(Base):
    """
    Class that represents locked out questions in the database.

    :param Base: ORM base class for database-derived classes.
    :type Base: sqlalchemy.ext.declarative.declarative_base
    """

    __tablename__ = "lockedout"

    lo_p_id = Column(Integer, primary_key=True, nullable=False)
    lo_q_id = Column(String, primary_key=True, nullable=False)
    lo_canchange = Column(Boolean)

    def __init__(self, lo_p_id, lo_q_id, lo_canchange):
        self.lo_p_id = lo_p_id
        self.lo_q_id = lo_q_id
        self.lo_canchange = lo_canchange

    def __repr__(self):  # pragma: no cover
        return (
            f"<Hamquestion(lo_p_id='{self.lo_p_id}', lo_q_id='{self.lo_q_id}', "
            f"lo_canchange='{self.lo_canchange}'>"
        )


# CREATE TABLE pool (p_id INTEGER PRIMARY KEY AUTOINCREMENT, p_publisher TEXT NOT NULL,
# p_pubid TEXT NOT NULL, p_name TEXT NOT NULL, p_formalname TEXT NOT NULL, p_version
# Column(Integer, nullable=False), p_start TEXT, p_end TEXT,
# p_type TEXT CHECK(p_type in ('hamquestion')), p_numq INT NOT NULL, p_pass INT NOT NULL,
# p_obsolete INT NOT NULL, p_complete BOOLEAN NOT NULL default FALSE, p_image TEXT,
# UNIQUE (p_publisher,p_pubid));


class Pool(Base):
    """
    Class to represent question pool data.

    :param Base: ORM base class for database-derived classes.
    :type Base: sqlalchemy.ext.declarative.declarative_base
    """

    __tablename__ = "pool"

    p_id = Column(Integer, primary_key=True, autoincrement=True)
    p_publisher = Column(String, nullable=False)
    p_pubid = Column(String, nullable=False)
    p_name = Column(String, nullable=False)
    p_formalname = Column(String, nullable=False)
    p_version = Column(Integer, nullable=False)
    p_start = Column(String)
    p_end = Column(String)
    p_type = Column(String, CheckConstraint("p_type in ('hamquestion')"))
    p_numq = Column(Integer, nullable=False)
    p_pass = Column(Integer, nullable=False)
    p_obsolete = Column(Integer, nullable=False)
    p_complete = Column(Boolean, nullable=False, default=False)
    p_image = Column(String)

    def __init__(
        self,
        p_id,
        p_publisher,
        p_pubid,
        p_name,
        p_formalname,
        p_version,
        p_start,
        p_end,
        p_type,
        p_numq,
        p_pass,
        p_obsolete,
        p_complete,
        p_image,
    ):
        self.p_id = p_id
        self.p_publisher = p_publisher
        self.p_pubid = p_pubid
        self.p_name = p_name
        self.p_formalname = p_formalname
        self.p_version = p_version
        self.p_start = p_start
        self.p_end = p_end
        self.p_type = p_type
        self.p_numq = p_numq
        self.p_pass = p_pass
        self.p_obsolete = p_obsolete
        self.p_complete = p_complete
        self.p_image = p_image

    def __repr__(self):  # pragma: no cover
        return (
            f"<Pool(p_id='{self.p_id}', p_publisher='{self.p_publisher}', "
            f"p_pubid='{self.p_pubid}', p_name='{self.p_name}', "
            f"p_formalname='{self.p_formalname}',p_version='{self.p_version}', "
            f"p_start='{self.p_start}', p_end='{self.p_end}', p_type='{self.p_type}', "
            f"p_numq='{self.p_numq}', p_pass='{self.p_pass}', "
            f"p_obsolete='{self.p_obsolete}', p_complete='{self.p_complete}', "
            f"p_image='{self.p_image}')>"
        )


# CREATE TABLE specs (s_p_id INTEGER, s_leadstrings TEXT, s_count INTEGER,
# PRIMARY KEY (s_p_id,s_leadstrings));


class Specs(Base):
    """
    Class to represent the exam specifications from the database. This is currently unused.
    This becomes useful if the exam format changes from the current one question from each
    leadstring group.
    TODO: Start using the data from this class.

    :param Base: ORM base class for database-derived classes.
    :type Base: sqlalchemy.ext.declarative.declarative_base
    """

    __tablename__ = "specs"

    s_p_id = Column(Integer, nullable=False)
    s_leadstrings = Column(String, primary_key=True, nullable=False)
    s_count = Column(Integer)

    def __init__(self, s_p_id, s_leadstrings, s_count):
        self.s_p_id = s_p_id
        self.s_leadstrings = s_leadstrings
        self.s_count = s_count

    def __repr__(self):  # pragma: no cover
        return (
            f"<Specs(s_p_id='{self.s_p_id}', "
            f"s_leadstrings='{self.s_leadstrings}', s_count='{self.s_count}')>"
        )
