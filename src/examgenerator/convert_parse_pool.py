#!/usr/bin/env python

"""
Parse an amateur radio question pool and populate the provided database
with the questions, answers, etc. from the pool.
"""

import contextlib
import re
import sys
from typing import Any, Dict, List, Tuple, Type

from . import custom_log
from .constants import EXAM_INDICES
from .globals import Question

mylog = custom_log.get_logger()


class ParsePool:
    """
    Consume a NCVEC question pool and return data structures with the questions,
    answers and distractors.
    """

    content_length: int
    file_content: List[str]
    _subele_start_lines: List[int] = []
    _question_start_lines: Dict[str, int] = {}

    subele_regexp = re.compile("^SUBELEMENT ([TGE][0-9]) - .*$")
    subsubele_regexp = re.compile("^([TGE][0-9][A-Z]) .*$")
    question_options_regex = re.compile(r"^([A-D])\. (.*)$")

    @classmethod
    def consume_pool(cls, inputfile: Any):
        """
        Convert supplied text file into a list of lines.

        :param inputfile: Question pool file.
        :type inputfile: Any
        :return: The content of the file stored as a list of lines.
        :rtype: List
        """

        if inputfile is sys.stdin:
            print("Expecting data on standard input...")
            _content = [line.strip() for line in sys.stdin]
        else:
            mylog.info(f"Input filename: {inputfile}")
            with open(inputfile, "r") as infile:
                _content = [line.strip() for line in infile]

        cls.file_content = _content

        # Remove header cruft as best we can.
        cls._prune_content()
        # Swap characters MSWord likes to insert to make documents pretty.
        cls._normalize_characters()
        # Fix oddities found in other editions
        cls._fix_anomalies()

    @classmethod
    def _prune_content(cls):
        """
        Filter to locate the beginning of the exam and trim the portion before that
        point from the list.

        Side-effect: Class-wide file_content list is filled with the filtered list
        with the beginning portion elided.
        """

        cls._question_start_lines.clear()
        cls._subele_start_lines.clear()

        found_exam_start_line = -1

        for line, _ in enumerate(cls.file_content):
            if cls.file_content[line].startswith(
                ("SUBELEMENT T1", "SUBELEMENT G1", "SUBELEMENT E1")
            ):
                found_exam_start_line = line

        # cls.file_content = [
        #     cls.file_content[item]
        #     for item in range(found_exam_start_line - 1, len(cls.file_content))
        # ]
        cls.file_content = cls.file_content[found_exam_start_line - 1 :]
        cls.content_length = len(cls.file_content)

    @classmethod
    def _normalize_characters(cls):
        """
        Filter to change several characters used for typesetting a document to
        visually-equivalent plain ASCII characters.

        Side-effect: Class-wide file_content list is altered by substituting characters.
        """
        conversions = {
            r"\N{EN DASH}": "-",
            r"\N{EM DASH}": "-",
            r"\N{NON-BREAKING HYPHEN}": "-",
            r"\N{RIGHT SINGLE QUOTATION MARK}": "'",
            r"\N{LEFT DOUBLE QUOTATION MARK}": '"',
            r"\N{RIGHT DOUBLE QUOTATION MARK}": '"',
            r"\N{NO-BREAK SPACE}": " ",
            r"\N{ZERO WIDTH NO-BREAK SPACE}": "",
            r"\N{VULGAR FRACTION ONE QUARTER}": "1/4",
            r"\N{VULGAR FRACTION ONE HALF}": "1/2",
        }

        for line, content in enumerate(cls.file_content):
            _tmp = "".join(chr(x) for x in content.encode("ASCII", "namereplace"))
            for bad_str, repl_str in conversions.items():
                if bad_str in _tmp:
                    _tmp = _tmp.replace(bad_str, repl_str)
            if cls.file_content[line] != _tmp:
                mylog.debug("Replacing:")
                mylog.debug(f"  {cls.file_content[line]}")
                mylog.debug("with:")
                mylog.debug(f"  {_tmp}")
                cls.file_content[line] = _tmp

    @classmethod
    def _fix_anomalies(cls):
        """
        Filter to address anomalies noticed in previous editions of the pools.

        Side-effect: Class-wide file_content list is altered in-place.
        """

        editorial_fixes = {
            "1/2 wavelength": "1/2-wavelength",
            "1/4 wavelength": "1/4-wavelength",
            "1/8 wavelength": "1/8-wavelength",
            "3/4 wavelength": "3/4-wavelength",
            "According the National Electrical": "According to the National Electrical",
        }

        altered_list = []
        assert cls.file_content
        for _line, _content in enumerate(cls.file_content):
            if _content.endswith(" ~~"):
                _content = _content.replace(" ~~", "")
                altered_list.append(_content)
                _content = "~~"
            elif (
                _content
                in [
                    "D. All these choices are correct",
                    "D. Using forward error correction",
                ]
                and cls.file_content[_line + 1] == ""
            ):
                altered_list.append(_content)
                _content = "~~"

            for orig, repl in editorial_fixes.items():
                if orig in _content:
                    _content = _content.replace(orig, repl)

            altered_list.append(_content)
        cls.file_content = altered_list

    @classmethod
    def collect_data_from_source(cls) -> Tuple[List, List, Dict, Dict]:
        """
        Parse the data contained in the supplied list into subelements, subsubelements,
        questions, question_data.

        :param content: Data pulled from the pool text file.
        :type content: list
        :return: A tuple of two lists and two dictionaries, specifically subelements,
            subsubelements, questions, question_data
        :rtype: Tuple[List, List, Dict, Dict]
        """

        # Start parsing the lines of text.
        subelements = cls._parse_subelements()
        subsubelements = cls._parse_subsubelements()

        questions = {
            subsubelement: cls._parse_question_ids(subsubelement)
            for subsubelement in subsubelements
        }

        question_data = {}
        for value in questions.values():
            for question in value:
                question_data[question] = cls._parse_question(question)

        return subelements, subsubelements, questions, question_data

    @classmethod
    def _parse_subelements(cls) -> list:
        """
        Extract a list of subelements from the supplied question list.

        :param filelines: Lines from which subelements are to be extracted.
        :type filelines: list
        :return: Subelements extracted from supplied list.
        :rtype: list
        """

        retval: List[str] = []
        for idx, line in enumerate(cls.file_content):
            if line.startswith(("SUBELEMENT T", "SUBELEMENT G", "SUBELEMENT E")):
                mylog.debug(f'Line: {line.encode("ASCII", "namereplace")}')
                with contextlib.suppress(TypeError):
                    retval.append(cls.subele_regexp.match(line).group(1))
                cls._subele_start_lines.append(idx)

        mylog.debug(f"Subelements found: {retval}")
        return retval

    @classmethod
    def _parse_subsubelements(cls) -> list:
        """
        Extract a list of sub-subelements from the supplied question list.

        :param filelines: Lines from which sub-subelements are to be extracted.
        :type filelines: list
        :return: Sub-subelements extracted from supplied list.
        :rtype: list
        """

        retval = []

        for idx, start_line in enumerate(cls._subele_start_lines):
            next_idx = cls.content_length
            with contextlib.suppress(IndexError):
                next_idx = cls._subele_start_lines[idx + 1]
            mylog.debug(
                "{}:\nlooking: {}-{}".format(
                    cls.file_content[start_line], start_line, next_idx
                )
            )
            for test_line in range(start_line, next_idx):
                mylog.debug(f"File line: {test_line}")
                with contextlib.suppress(AttributeError):
                    with contextlib.suppress(TypeError):
                        retval.append(
                            cls.subsubele_regexp.match(
                                cls.file_content[test_line]
                            ).group(1)
                        )
                        continue
        mylog.debug(f"Subsubelements found: {retval}")
        return retval

    @classmethod
    def _parse_question_ids(cls, subelement: str) -> list:
        """
        Extract question identifiers from the supplied list that belong to the supplied subelement.

        :param filelines: Lines from which subelements are to be extracted.
        :type filelines: list
        :param subelement: The subelement to which the question identifiers must belong.
        :type subelement: str
        :return: The extracted identifiers
        :rtype: list
        """

        retval = []
        regexp = re.compile(f"^({subelement}[0-9]+) .*$")

        for idx, start_line in enumerate(cls._subele_start_lines):
            next_idx = cls.content_length
            with contextlib.suppress(IndexError):
                next_idx = cls._subele_start_lines[idx + 1]
            for test_line in range(start_line, next_idx):
                with contextlib.suppress(AttributeError):
                    with contextlib.suppress(TypeError):
                        _question = regexp.match(cls.file_content[test_line])[1]
                        retval.append(_question)
                        cls._question_start_lines[_question] = test_line
        return retval

    @classmethod
    def _parse_question(cls, question: str) -> Question:
        """
        Extract a specific question from the list of lines.

        :param filelines: Lines from which the question is to be extracted.
        :type filelines: list
        :param question: Question identifier to be extracted.
        :type question: str
        :return: An instance of the Question class with the extracted data.
        :rtype: Question
        """

        found_question_start = False

        question_answer = re.compile(rf"^{question} \(([A-D])\).*$")

        answer = ""
        choices = {}
        text = ""
        for line_no in range(cls._question_start_lines[question], cls.content_length):
            line = cls.file_content[line_no]
            if found_question_start:
                # Stop when we hit the single, double- or quadruple-tilde
                # signifying the end of a question or the pool.
                if "~" in line:
                    mylog.debug(f"End of question {question}.")
                    break
                # Second and remaining lines of the question
                if not cls.question_options_regex.match(line):
                    text = f"{text} {line}".strip()
                    mylog.debug(f"Question text: {text}")
                else:
                    mylog.debug("Parsing responses.")
                    mylog.debug(f"{line}")
                    if cls.question_options_regex.match(line) is not None:
                        choice, choice_text = cls.question_options_regex.match(
                            line
                        ).group(1, 2)
                        if choice in ["A", "B", "C", "D"]:
                            choices[choice] = choice_text
            elif question_answer.match(line):
                mylog.debug(f"Line: {line}")
                answer = question_answer.match(line).group(1)
                mylog.debug(f"Start of question {question}.")
                mylog.debug(f"Grabbed answer: {answer}")
                found_question_start = True

        pool_id = EXAM_INDICES[question[0]]
        try:
            return Question(
                pool_id,
                question,
                text,
                answer,
                choices["A"],
                choices["B"],
                choices["C"],
                choices["D"],
            )
        except KeyError:
            mylog.error(f"Error parsing question {question}")
            raise KeyError from None
