"""
Script to create and populate an exam database.
"""

from typing import Dict, List

from sqlalchemy import create_engine, text
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import Session

from . import custom_log
from .constants import EXAM_INDICES
from .convert_db_schema import POOL_CONTENTS,Base, Hamquestion, Pool, Specs

mylog = custom_log.get_logger()


def populate_database(
    outputfile: str,
    subelements: List,  # pylint: disable=unused-argument
    subsubelements: List,
    questions: Dict,  # pylint: disable=unused-argument
    question_data: Dict,
) -> None:
    """
    Create a database and populate it with the supplied subelements, subsubelements,
    questions and question_data.

    :param outputfile: The file name of the new database file.
    :type outputfile: str
    :param subelements: List of subelements of the exam.
    :type subelements: List
    :param subsubelements: List of sub-subelements of the exam.
    :type subsubelements: List
    :param questions: Question identifiers in the exam.
    :type questions: Dict
    :param question_data: Question data for each question in the exam.
    :type question_data: Dict
    """

    # Create a reference to the desired database for SQLAlchemy
    engine = create_engine(f"sqlite:///{outputfile}")
    Base.metadata.create_all(engine)

    # Database is created, obtain a connection and start populating.
    session = Session(engine)

    # See if the table pool already contains data.
    dbbase = automap_base()
    dbbase.prepare(engine)
    dbsession = Session(engine)
    pool = dbbase.classes.pool
    thing = dbsession.query(pool).order_by(text("p_id")).all()
    if len(thing) == 0:
        _populate_pool(session)

    # Populate Specs table
    _populate_specs(session, subsubelements)

    # Populate Hamquestion table
    _populate_hamquestion(session, question_data)


def _populate_hamquestion(session: Session, question_data: Dict) -> None:
    """
    Populate the database with the supplied questions.

    :param session: Database session to operate on.
    :type session: Session
    :param question_data: The question data to insert into the database.
    :type question_data: Dict
    """

    for question in question_data.values():
        ham_q = Hamquestion.add_question(question)
        ham_q.q_p_id = EXAM_INDICES[question.q_id[0]]
        session.add(ham_q)

    # Write these changes to disk.
    session.commit()


def _populate_pool(session: Session) -> None:
    """
    Populate the pool table with the metadata from the exam.

    :param session: Database session to operate on.
    :type session: Session
    """

    for value in POOL_CONTENTS.values():
        content_list = value.split("|")

        pool = Pool(
            p_id=content_list[0],
            p_publisher=content_list[1],
            p_pubid=content_list[2],
            p_name=content_list[3],
            p_formalname=content_list[4],
            p_version=content_list[5],
            p_start=content_list[6],
            p_end=content_list[7],
            p_type=content_list[8],
            p_numq=content_list[9],
            p_pass=content_list[10],
            p_obsolete=content_list[11],
            p_complete=(content_list[12] == 1),
            p_image=content_list[13],
        )

        session.add(pool)

    # Write these changes to disk.
    session.commit()


def _populate_specs(session: Session, subsubelements: List) -> None:
    """
    Populate the specs table with the exam specifications.

    :param session: Database session to operate on.
    :type session: Session
    :param subsubelements: The sub-subelements to insert into the database.
    :type subsubelements: List
    """

    # Sub-sub-elements are "Lead Strings" in the database.
    for subsube in subsubelements:
        examnum = EXAM_INDICES[subsube[0]]
        spec = Specs(s_p_id=examnum, s_leadstrings=subsube, s_count=1)
        session.add(spec)

    session.commit()
