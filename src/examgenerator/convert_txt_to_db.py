#!/usr/bin/env python

"""
A script to convert the published NCVEC question pools to the ARRL database format used
in the generator.
"""

import argparse
import contextlib
import logging
import os
import sys

import pkg_resources

from . import custom_log
from .convert_parse_pool import ParsePool
from .convert_populate_db import populate_database

mylog = custom_log.get_logger()


def do_conversion() -> None:
    """Perform conversion of the supplied text question pool to a database."""

    # Parse the command-line arguments, if any.
    args = arg_parser()
    handle_args(args)

    # Read the input file / stdin line by line into a list
    for source in args.inputfile:
        ParsePool.consume_pool(source)

        # Using that list, extract the data into useful structures
        (
            subelements,
            subsubelements,
            questions,
            question_data,
        ) = ParsePool.collect_data_from_source()

        # The information is now in a useful form, let's create a database around it.
        populate_database(
            args.outputfile, subelements, subsubelements, questions, question_data
        )


def arg_parser():
    """
    Populate the arguments accepted by the application.

    :return: The customized parser output
    :rtype: argparse.Namespace
    """

    mylog.debug(f"Number of arguments: {len(sys.argv)}")

    # pylint: disable=protected-access
    debug_opt_list = list(logging._nameToLevel)
    debug_opt_list.extend(str(value) for value in logging._nameToLevel.values())
    argp = argparse.ArgumentParser(description="An amateur radio exam importer.")
    argp.add_argument("-f", "--outputfile", type=str, default=None, required=True)
    argp.add_argument("inputfile", nargs="*", type=str, default=None)
    argp.add_argument(
        "-v",
        dest="debug",
        action="store_true",
        help="Turn on verbose debugging output.",
    )
    argp.add_argument(
        "-V",
        dest="version",
        action="store_true",
        help="Display application version and exit.",
    )
    return argp.parse_args()


def handle_args(args):
    """
    Handle some of the actions as appropriate given the arguments supplied to the application.

    :param args: Supplied arguments
    :type args: dict
    """

    # Handle the version flag.
    try:
        mylog.debug("Version argument: %s is %s", args.version, type(args.version))
        if args.version:  # pragma: no cover
            _version = "Not packaged."
            with contextlib.suppress(pkg_resources.DistributionNotFound):
                _version = pkg_resources.get_distribution("Examgenerator").version
            if _version == "Not packaged." and os.path.exists("pyproject.toml"):
                with open("pyproject.toml") as toml:
                    for _tmp in toml:
                        if "version" in _tmp:
                            _version = _tmp.replace("version =", "").strip().strip('"')
                            break

            print("An amateur radio exam generator.")
            print(f"Version: {_version}")
            sys.exit()
    except ValueError:  # pragma: no cover
        mylog.debug("Debug argument is not valid")
        # But ignore it.

    # Handle the debugging flag.
    try:
        mylog.debug("Debug argument: %s is %s", args.debug, type(args.debug))
        if args.debug:
            logging.getLogger(__package__).setLevel(logging.DEBUG)
            mylog.debug("Debugging enabled.")
    except ValueError:  # pragma: no cover
        mylog.debug("Debug argument is not valid")
        # But ignore it.


if __name__ == "__main__":
    sys.exit(do_conversion())  # pragma: no cover
