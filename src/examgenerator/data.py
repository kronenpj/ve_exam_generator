"""
Provide an interface to store & retrieve information from the database.
"""

import contextlib
import logging
import os
from typing import Dict

from sqlalchemy import create_engine, exc, text
from sqlalchemy.engine.base import Engine
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import Session

from . import custom_log
from .constants import EXAMDB_FILE

# sqllog = logging.getLogger("sqlalchemy.engine")
# sqllog.setLevel(logging.DEBUG)

mylog = custom_log.get_logger()


class Data:
    """A shared-state Class to encapsulate interactions with the exam database.
    see http://web.archive.org/web/http://code.activestate.com/recipes/66531/
    """

    # Create a reference to the desired database for SQLAlchemy
    engine_file: str = ""
    engine: Engine
    dbbase = None
    dbsession: Session
    hamquestion = None
    lockedout = None
    pool = None
    specs = None
    mylog: logging.Logger

    def __new__(cls, engine_file: str = EXAMDB_FILE):
        if not hasattr(cls, "instance"):
            cls.mylog = mylog
            cls.instance = super(Data, cls).__new__(cls)
            cls.instance.set_database_file(engine_file)
        return cls.instance

    def set_database_file(self, engine_file: str) -> None:
        """
        Specify the database to use for the application.

        :param engine_file: The path / name of the database file.
        :type engine_file: str
        """
        if self.engine_file == engine_file:
            return
        self.engine_file = engine_file

        with contextlib.suppress(Exception):
            self.dbsession.close()
        # Invalidate current engine
        self.engine = None
        self.dbbase = None
        self.dbsession = None
        self.hamquestion = None
        self.lockedout = None
        self.pool = None
        self.specs = None

    def connect(self) -> None:
        """
        Open the database connection.

        :raises exc.ArgumentError: Thrown exception is the database filename / path
            is not found or unreachable.
        """
        if self.engine_file is not None and os.path.exists(self.engine_file):
            self.engine = create_engine(f"sqlite:///{self.engine_file}")
        else:
            self.mylog.debug("Database file has not been set.")
            # I'm co-opting this exception to indicate the database file is None.
            raise exc.ArgumentError()

        self.dbbase = automap_base()

        # reflect the tables
        self.dbbase.prepare(self.engine)

        # Start a database session
        self.dbsession = Session(self.engine)

        # mapped classes are now created with names by default matching that of the
        # table name.
        self.hamquestion = self.dbbase.classes.hamquestion
        self.lockedout = self.dbbase.classes.lockedout
        self.pool = self.dbbase.classes.pool
        self.specs = self.dbbase.classes.specs
        # These exist in the ARRL version of the database but this program doesn't
        # use them.
        # self.graphics = self.dbbase.classes.graphics
        # self.metadata = self.dbbase.classes.metadata  # pragma: no mutate

    def pools(self) -> Dict[str, str]:
        """
        Return all pool identifiers and names.

        :return: Dictionary of available pool identifiers in the database.
        :rtype: dict
        """
        if self.engine is None:
            self.connect()
        thing = self.dbsession.query(self.pool).order_by(text("p_id")).all()
        return {item.p_id: item.p_name for item in thing}

    def pool_title(self, pool: int) -> str:
        """
        Convert a pool index into a pool title.

        :param pool: Index of the pool to query.
        :type pool: int
        :return: Description of the supplied pool index.
        :rtype: str
        """
        if self.engine is None:  # pragma: no cover
            self.connect()
        thing = self.dbsession.query(self.pool).filter(self.pool.p_id == pool).all()
        pools = ""
        for item in thing:
            pools = item.p_formalname
        return pools

    def pool_questions(self, pool: int) -> int:
        """
        Return the number of expected questions for the provided pool.

        :param pool: Index of the pool to be queried.
        :type pool: int
        :return: Number of questions in an exam generated from the supplied pool.
        :rtype: int
        """
        if self.engine is None:
            self.connect()
        thing = self.dbsession.query(self.pool).filter(self.pool.p_id == pool)
        return thing[0].p_numq

    def specs_all_leadstrings(self) -> dict:
        """
        Return all 'lead strings' in all pools.

        :return: Dictionary of the lead strings from all pools in the database.
        :rtype: dict
        """
        if self.engine is None:
            self.connect()
        thing = (
            self.dbsession.query(self.specs).order_by(text("s_p_id")).all()
        )  # pragma: no mutate
        return {item.s_leadstrings: item.s_count for item in thing}  # pragma: no mutate

    def specs_pool_leadstrings(self, pool: int) -> dict:
        """
        Return 'lead strings' for the requested pool.

        :param pool: Index of the pool to be queried.
        :type pool: int
        :return: Dictionary of lead strings from that pool.
        :rtype: dict
        """
        if self.engine is None:  # pragma: no cover
            self.connect()
        thing = (
            self.dbsession.query(self.specs)
            .filter_by(s_p_id=pool)
            .order_by(text("s_leadstrings"))
            .all()
        )
        return {item.s_leadstrings: item.s_count for item in thing}

    def all_lockedout_questions(self) -> list:
        """
        Return all questions listed as locked-out.

        :return: List of locked-out questions.
        :rtype: list
        """
        if self.engine is None:  # pragma: no cover
            self.connect()
        self.mylog.debug("Gathering all locked-out questions")
        thing = self.dbsession.query(self.lockedout).all()
        return [item.lo_q_id for item in thing]

    def lockedout_questions(self, pool: int) -> list:
        """
        Return questions listed as locked-out for a supplied pool.

        :param pool: Pool index to query.
        :type pool: int
        :return: List of questions locked-out for that pool.
        :rtype: list
        """
        if self.engine is None:  # pragma: no cover
            self.connect()
        self.mylog.debug(f"Looking for locked-out questions in pool {pool}")
        thing = (
            self.dbsession.query(self.lockedout)
            .filter(self.lockedout.lo_p_id == pool)
            .all()
        )
        return [item.lo_q_id for item in thing]

    def questions_leadstring(self, leadstring: str) -> list:
        """
        Return questions in the requested 'lead string' group.

        :param leadstring: The leadstring to query.
        :type leadstring: str
        :return: List of question identifiers belonging to that leadstring group.
        :rtype: list
        """
        if self.engine is None:  # pragma: no cover
            self.connect()
        self.mylog.debug(f"Looking for {leadstring}%")
        thing = (
            self.dbsession.query(self.hamquestion)
            .filter(self.hamquestion.q_id.like(f"{leadstring}%"))
            .order_by(text("q_id"))
            .all()
        )
        return [item.q_id for item in thing]

    def question_data(self, question_id: str) -> dict:
        """
        Return requested question in a dictionary.

        :param question_id: Question identifier to query.
        :type question_id: str
        :return: Dictionary containing the data for that question.
        :rtype: dict
        """
        if self.engine is None:  # pragma: no cover
            self.connect()
        self.mylog.debug(f"Gathering data for {question_id}%")
        thing = (
            self.dbsession.query(self.hamquestion)
            .filter(self.hamquestion.q_id == question_id)  # pragma: no mutate
            .all()
        )
        self.mylog.debug(f"thing: <q_text={thing[0].q_text}, q_ans={thing[0].q_ans}>")

        # Construct the response
        return {
            "text": thing[0].q_text.strip(),
            "ans": thing[0].q_ans.strip(),
            "a": thing[0].q_a.strip(),
            "b": thing[0].q_b.strip(),
            "c": thing[0].q_c.strip(),
            "d": thing[0].q_d.strip(),
        }
