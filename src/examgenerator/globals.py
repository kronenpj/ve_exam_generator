"""
Global information to be used by the module.
"""
from typing import Dict

from .shuffle_answers import abcd_generator, dcba_generator, random_generator

# Shared globals are explained here:
# https://docs.python.org/3/faq/programming.html#how-do-i-share-global-variables-across-modules

RAND_TYPES: Dict[str, object] = {
    "NONE": None,
    "RANDOM": random_generator,
    "ABCD": abcd_generator,
    "DCBA": dcba_generator,
}


class Question:
    """
    Class to hold data related to a single question from the pool.
    This data structure is used by the application vs. the one
    specific to the database, although they are similar.
    """

    q_p_id: int
    q_id: str
    q_text: str
    q_answer: str
    q_a: str
    q_b: str
    q_c: str
    q_d: str

    def __init__(
        self,
        q_p_id,
        q_id,
        q_text,
        q_answer,
        q_a,
        q_b,
        q_c,
        q_d,
    ) -> None:
        self.q_p_id = q_p_id
        self.q_id = q_id
        self.q_text = q_text
        self.q_answer = q_answer
        self.q_a = q_a
        self.q_b = q_b
        self.q_c = q_c
        self.q_d = q_d

    def __str__(self) -> str:  # pragma: no cover
        """
        Return a string representation of the contents of the class attributes.

        :return: String representation of the contents of the class attributes.
        :rtype: str
        """
        temp = f"    Pool ID: {self.q_p_id}\n"
        temp = f"{temp}Question ID: {self.q_id}\n"
        temp = f"{temp}  Text: {self.q_text}\n"
        temp = f"{temp}Answer: {self.q_answer}\n"
        temp = f"{temp}     A: {self.q_a}\n"
        temp = f"{temp}     B: {self.q_b}\n"
        temp = f"{temp}     C: {self.q_c}\n"
        temp = f"{temp}     D: {self.q_d}"
        return temp
