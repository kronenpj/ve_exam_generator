"""
Output the exam formatted by the supplied Jinja2 template.
"""
import os
from typing import TextIO

import jinja2

from . import custom_log
from .constants import ANS_LIST_CONVERSION, LIST_ANS_CONVERSION
from .data import Data
from .shuffle_answers import shuffle_answer

mylog = custom_log.get_logger()


def output_exam_jinja2(
    outstream: TextIO,
    pool: int,
    questions: list,
    examnumber: int,
    generator=None,
    template: str = "NONE",
    jinja_file: str = "text",
) -> None:  # pragma: no cover
    """Compose an exam from the selected pool and output in the format specified
    by the Jinja2 template."""

    jinja_env = jinja2.Environment(
        block_start_string=r"\BLOCK{",
        block_end_string="}",
        variable_start_string=r"\VAR{",
        variable_end_string="}",
        comment_start_string=r"\#{",
        comment_end_string="}",
        line_statement_prefix="%%",
        line_comment_prefix="%#",
        trim_blocks=True,
        autoescape=False,
        loader=jinja2.FileSystemLoader(os.path.abspath(".")),
    )

    data = Data()

    gen = generator(4) if generator else None

    title = data.pool_title(pool)
    question_dict = {}
    q_id_dict = {}
    response_dict = {}
    answers = {}
    # Build the data structures used by the template.
    for qnum, qid in enumerate(questions, start=1):
        mylog.debug("Retrieving data for question ID: %s", qid)
        q_data = data.question_data(qid)
        text = q_data["text"].replace("$", r"\$").replace("#", r"\#").strip()
        question_dict[qnum] = text
        q_id_dict[qnum] = qid

        correct_answer = ANS_LIST_CONVERSION[q_data["ans"]]
        answer_list = [
            q_data[item].replace("$", r"\$").replace("#", r"\#")
            for item in ["a", "b", "c", "d"]
        ]

        if gen:
            correct_answer, answer_list = shuffle_answer(
                answer_list, correct_answer, gen
            )
        response_dict[qnum] = {
            item: answer_list[ANS_LIST_CONVERSION[item.upper()]]
            for item in ["a", "b", "c", "d"]
        }

        answers[qnum] = LIST_ANS_CONVERSION[correct_answer]

    j2_template = jinja_env.get_template(jinja_file)
    outstring = j2_template.render(
        title=title,
        valid_exam_text="PLEASE DO NOT WRITE IN THIS BOOKLET"
        if template not in ["ABCD", "DCBA"]
        else "NOT A VALID AMATEUR RADIO EXAM",
        exam_template=f'Exam template: {template.replace("TEMP", "")}'
        if template not in ["NONE", "ABCD", "DCBA", "RANDOM"]
        else "",
        exam_number=examnumber,
        qdata=question_dict,
        qid=q_id_dict,
        adata=response_dict,
        correct=answers,
    )

    outstream.write(outstring)
