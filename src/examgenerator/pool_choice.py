"""
Choose a question pool based on command line arguments
"""


import contextlib
import sys
from typing import Any, Dict

from . import custom_log

mylog = custom_log.get_logger()


def determine_pool(args, pools: Dict[int, str]) -> int:
    """Figure out what pool to use to generate an exam."""

    # Figure out what exam to generate...
    try:
        use_pool = _parse_pool_args(args, pools)
    except AssertionError:  # pragma: no cover
        use_pool = -1
    if use_pool == -1:
        use_pool = _prompt_user(pools)
    # If we didn't come up with a valid pool, notify the user and exit.
    if use_pool == -1:
        choice_list = [str(x) for x in pools]
        choice_list.extend(x.lower()[0] for x in pools.values())
        print("ERROR: That is not a valid selection.")
        print(f"Valid choices are: {choice_list}")
        sys.exit(1)
    return use_pool


def _parse_pool_args(args, pools: Dict[int, str]) -> int:
    choice: int = -1

    mylog.debug("Pool argument: %s is %s", args.pool, type(args.pool))
    try:
        choice = _check_choice(args.pool[0], pools)
    except (ValueError, IndexError, TypeError):
        mylog.debug("Pool argument is not valid")

    try:
        mylog.debug("After parsing, pool choice is %s.", choice)
    except (ValueError, UnboundLocalError):
        mylog.debug("Choice is undefined.")
        return -1

    return choice


def _get_input(text: str) -> Any:
    """An input function that can be mocked."""
    return input(text)


def _check_choice(choice: Any, pools: Dict[int, str]) -> int:
    """Basic input validation / parameter interpretation."""

    with contextlib.suppress(ValueError, KeyError, TypeError):
        # Try to convert the given choice to a number
        choice = int(choice)
        mylog.debug("Choice: %s is %s", choice, type(choice))

    # If choice matches one of the values in pools, return it.
    if choice in pools:
        mylog.debug("Returning: %s", choice)
        return choice

    # Try to interpret the given choice in the context of the pool names
    try:
        # Reverse the pools dictionary
        poolindex = {v[0].lower(): k for k, v in pools.items()}
        mylog.debug("Poolindex: %r", poolindex)

        # Gather valid first characters from pool names.
        names = [i.lower()[0] for i in pools.values()]
        mylog.debug("Names: %r", names)

        # Check to see if the examchoice is in that first characters list
        if choice.lower()[0] in names:
            choice = poolindex[choice.lower()[0]]

        # Try to use the given choice
        _ = pools[choice]  # pragma: no mutate
    except (
        ValueError,
        KeyError,
        TypeError,
        IndexError,
        AttributeError,
    ):  # pragma: no cover
        return -1

    mylog.debug("Returning: %s", choice)
    return choice


def _prompt_user(pools: Dict[int, str]) -> int:
    """Prompt the user as a last resort."""

    print("Available exams:")
    for item, value in pools.items():
        print(f"{item}. {value}")
    print()
    try:
        choice_list = [str(x) for x in pools]
        choice_list.extend(x.lower()[0] for x in pools.values())
        examchoice = _get_input(f"Enter exam to generate {choice_list}: ")
    except (KeyboardInterrupt, EOFError):  # pragma: no cover
        print()
        print("Exiting...")
        sys.exit(0)

    examchoice = _check_choice(examchoice, pools)
    return examchoice
