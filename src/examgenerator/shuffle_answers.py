"""
Routines to shuffle the answers & destractors in various ways.
"""

from random import shuffle
from typing import Iterable, Tuple

from . import custom_log
from .constants import ANS_LIST_CONVERSION

mylog = custom_log.get_logger()


def abcd_generator(responses: int) -> Iterable[int]:  # pylint: disable=unused-argument
    """A generator which returns and endless sequence ."""

    responselist = list(ANS_LIST_CONVERSION.values())
    while 1:
        yield from responselist


def dcba_generator(responses: int) -> Iterable[int]:  # pylint: disable=unused-argument
    """A generator which returns random numbers between 0 and responses."""

    responselist = list(ANS_LIST_CONVERSION.values())
    responselist.reverse()
    while 1:
        yield from responselist


def random_generator(responses: int) -> Iterable[int]:
    """A generator which returns numbers between 0 and responses from a shuffled list."""

    templist = list(range(responses))
    while 1:
        responselist = templist
        shuffle(responselist)
        yield from responselist


def shuffle_answer(
    responses: list, answer: int, generator: Iterable[int]
) -> Tuple[int, list]:
    """Given a list of responses, the correct response and a correct response generator,
    return the new answer position and a shuffled response list.
    NOTE: The generator must be initialized before passing into this function!
    """

    shuffled_list = []
    # Obtain the correct answer position from the generator
    new_answer = next(generator)
    mylog.debug("New answer index: %s", new_answer)

    # Capture and remove the correct answer from the original list.
    desired_answer = responses.pop(answer)
    mylog.debug("Desired answer: %s", desired_answer)
    mylog.debug("Remaining responses: %s", responses)

    # Shuffle the remaining answers (distractors)
    distractors = list(responses)
    shuffle(distractors)
    mylog.debug("Shuffled distractors: %s", distractors)

    # Reassemble the response list.
    for index in range(len(responses) + 1):
        mylog.debug("Current index: %d", index)
        # Re-insert the remainder of the distrators, except...
        if index != new_answer:
            mylog.debug("Distractor list: %r", distractors)
            distractor = distractors.pop()
            mylog.debug("Appending distractor: %s to return list.", distractor)
            shuffled_list.append(distractor)
        else:
            # When the new_answer index comes around, then insert the correct answer.
            mylog.debug("Appending desired answer: %s to return list.", desired_answer)
            shuffled_list.append(desired_answer)
    mylog.debug("Final shuffled list: %r", shuffled_list)

    return new_answer, shuffled_list
