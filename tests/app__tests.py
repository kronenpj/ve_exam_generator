"""
Tests for the application module.
"""
import os
import random
from tempfile import mkstemp
from unittest import mock

import pytest

from examgenerator.app import application

# pylint: disable=invalid-name

valid_pairs = {
    "1": "Technician",
    "2": "General",
    "3": "Extra",
    "t": "Technician",
    "g": "General",
    "e": "Extra",
    "T": "Technician",
    "G": "General",
    "E": "Extra",
}

rand_generators = ["ABCD", "abcd", "DCBA", "dcba"]
output_types = ["text.j2", "html.j2", "latex.j2"]

pool_args = ["-p", "--pool="]
seed_args = ["-s", "--seed="]
# Seed #0 is "special" even though it's otherwise a valid seed.
seed_values = [1, 254, 65536, 38572, 2319457662, 1726153]

if os.path.exists("/tmp") and os.path.isdir("tmp"):
    TEMP_PATH = "/tmp"
else:
    TEMP_PATH = None


@pytest.mark.parametrize(
    "pool_arg, pool, pool_name",
    [(x, y, z) for x in pool_args for y, z in valid_pairs.items()],
)
def test_cmdline_pool_spec(pool_arg: str, pool: str, pool_name: str):
    fd, path = mkstemp(dir=TEMP_PATH)
    with mock.patch("sys.argv", [""] + [f"-f{path}"] + [f"{pool_arg}{pool}"]):
        application()
    with open(fd, "r") as f:
        captured: str = f.read()
    os.unlink(path)
    assert pool_name in captured


@pytest.mark.parametrize(
    "seed_arg, seed_value", [(x, y) for x in seed_args for y in seed_values]
)
def test_cmdline_seed_spec(seed_arg, seed_value):
    pool_list: list = list(valid_pairs.keys())
    random.shuffle(pool_list)

    pool = pool_list.pop()
    fd, path = mkstemp(dir=TEMP_PATH)
    with mock.patch(
        "sys.argv", [""] + [f"-f{path}", f"-p{pool}", f"{seed_arg}{seed_value}"]
    ):
        application()
    with open(fd, "r") as f:
        captured: str = f.read()
    os.unlink(path)
    assert f"Exam number: {seed_value}" in captured
    assert f"{valid_pairs[pool]}" in captured


@pytest.mark.parametrize("pool, pool_name", valid_pairs.items())
def test_user_prompt(pool: str, pool_name: str):
    fd, path = mkstemp(dir=TEMP_PATH)
    with mock.patch("sys.argv", [""] + [f"-f{path}"]), mock.patch(
        "examgenerator.pool_choice._get_input", return_value=pool
    ):
        application()
    with open(fd, "r") as f:
        captured: str = f.read()
    os.unlink(path)
    assert pool_name in captured


def test_stdout(capsys):
    with mock.patch("sys.argv", [""] + ["-f-"] + ["-p1"]):
        application()
    captured, __ = capsys.readouterr()
    assert "Technician" in str(captured)


def test_missing_dbfile(capsys):
    with pytest.raises(SystemExit):
        with mock.patch("examgenerator.constants.EXAMDB_FILE", None), mock.patch(
            "sys.argv", [""] + ["-d", "nodatabase", "-p", "1"]
        ):
            application()
    captured, __ = capsys.readouterr()
    assert "Database nodatabase is not an existing file." in str(captured)
