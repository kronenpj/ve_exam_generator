"""
Tests the compose_exam_list module.
"""
from unittest import mock

import pytest

from examgenerator.compose_exam import _choose_random, compose_exam_list
from examgenerator.constants import EXAMDB_FILE
from examgenerator.data import Data

# pylint: disable=missing-function-docstring

valid_q_list = {
    1: "T",
    2: "G",
    3: "E",
}


def my_chooser(items: list) -> str:
    return items[0]


@pytest.mark.parametrize("pool", list(valid_q_list))
def test_compose_exam(pool):
    with mock.patch(
        "examgenerator.compose_exam._choose_random", side_effect=my_chooser
    ):
        qlist = compose_exam_list(pool)
        for question in qlist:
            # print(f"Q in list: {question}")
            assert f"{question}".endswith("01")


def _chooser_w_lockout(collection: list) -> str:
    data = Data()
    data.set_database_file(EXAMDB_FILE)
    locked_list = data.all_lockedout_questions()

    for question in locked_list:
        # question[:3] a potential key for the collection dict
        if question[:3] in collection[0]:
            print(f"{question} not in {collection}")
            assert question not in collection
    return _choose_random(collection)


def test_lockedout_questions():
    data = Data()
    data.set_database_file(EXAMDB_FILE)

    pools = data.pools()

    for pool in pools:
        with mock.patch(
            "examgenerator.compose_exam._choose_random", side_effect=_chooser_w_lockout
        ):
            compose_exam_list(pool)
