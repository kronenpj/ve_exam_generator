"""
Tests for the convert text to database module.
"""
import pytest

import examgenerator.convert_parse_pool as cpp

TEXT_SOURCE_FILES = [
    "2022-2026-Tech-Pool.txt",
    "2023-2027-General-Pool.txt",
    "2024-2028-Extra-Pool.txt",
]


@pytest.mark.parametrize("source", TEXT_SOURCE_FILES)
def test_verify_parse_quantities(source):
    """
    Test to assure the size of the data collected is approximately correct.
    """
    cpp.ParsePool.consume_pool(source)

    (
        subelements,
        subsubelements,
        question_ids,
        __,
    ) = cpp.ParsePool.collect_data_from_source()
    # Compare with the original database.
    assert len(subelements) == 10
    assert len(subsubelements) > 30
    _tmp = [_new for _sublist in question_ids.values() for _new in _sublist]
    # print("Question IDs: {}".format(_tmp))
    assert len(_tmp) > 300
