"""
Tests for the convert text to database module.
"""
import os
import sys  # pylint: disable=unused-import
from typing import List
from unittest import mock

import pytest
from Levenshtein.StringMatcher import StringMatcher

import examgenerator.convert_parse_pool as cpp
from examgenerator import do_conversion
from examgenerator.constants import EXAMDB_FILE
from examgenerator.data import Data

# pylint: disable=missing-function-docstring

# Mark entire test package as being 'slow' to run.
pytestmark = pytest.mark.slow

try:
    PYENV: str = os.getenv("TOX_ENV_NAME", default="none")
except ValueError:
    PYENV = "none"
# sys.stderr.write(f"test_convert_txt_to_db:PYENV: {PYENV}")
TEST_DATABASE_FILE = f"testExamData-{PYENV}.db"
TEXT_SOURCE_FILES = [
    "2022-2026-Tech-Pool.txt",
    "2023-2027-General-Pool.txt",
    "2024-2028-Extra-Pool.txt",
]
LOCKEDOUT_QUESTIONS = "lockedout.txt"


def gather_lockedout() -> List[str]:
    retval = []
    with open(LOCKEDOUT_QUESTIONS, "r") as in_file:
        retval = [line.strip() for line in in_file]
    return retval


@pytest.mark.parametrize("source", TEXT_SOURCE_FILES)
def test_orchestrate(source: str):
    cpp.ParsePool.consume_pool(source)
    _verify_questions_against_db()
    _verify_sub_sub_elements_against_db()

    if os.path.exists(TEST_DATABASE_FILE):
        os.remove(TEST_DATABASE_FILE)

    with mock.patch("sys.argv", [""] + ["-f", TEST_DATABASE_FILE] + [source]):
        do_conversion()

    _populate_database()


# This takes a little while.
def _verify_questions_against_db():
    lev_distance = 4

    lev = StringMatcher()

    lockedout = gather_lockedout()

    (__, __, __, q_data) = cpp.ParsePool.collect_data_from_source()
    # Compare with the original database.
    data = Data()
    data.set_database_file(EXAMDB_FILE)
    for q_number, question_txt in q_data.items():
        try:
            # print(f"question: {question_txt}")
            database = data.question_data(q_number)
            lev.set_seqs(question_txt.q_text, database["text"])
            if lev.distance() > lev_distance:
                assert (
                    question_txt.q_text == database["text"]
                ), f"Error: Question {q_number}'s text(-) does not match the database(+) ({lev.distance()})."

            assert (
                question_txt.q_answer == database["ans"]
            ), f"Error: Question {q_number}'s answer(-) does not match the database(+)."

            lev.set_seqs(question_txt.q_a, database["a"])
            if lev.distance() > lev_distance:
                assert (
                    question_txt.q_a == database["a"]
                ), f"Question {q_number}, choice A does not match ({lev.distance()})."

            lev.set_seqs(question_txt.q_b, database["b"])
            if lev.distance() > lev_distance:
                assert (
                    question_txt.q_b == database["b"]
                ), f"Question {q_number}, choice B does not match ({lev.distance()})."

            lev.set_seqs(question_txt.q_c, database["c"])
            if lev.distance() > lev_distance:
                assert (
                    question_txt.q_c == database["c"]
                ), f"Question {q_number}, choice C does not match ({lev.distance()})."

            lev.set_seqs(question_txt.q_d, database["d"])
            if lev.distance() > 15:
                assert (
                    question_txt.q_d == database["d"]
                ), f"Question {q_number}, choice D does not match ({lev.distance()})."
        except IndexError:
            if q_number not in lockedout:
                pytest.fail(f"Question '{q_number}' does not exist in the database.")


def _verify_sub_sub_elements_against_db():
    (__, subsubels, __, __) = cpp.ParsePool.collect_data_from_source()
    # Compare with the original database.
    data = Data()
    data.set_database_file(EXAMDB_FILE)
    leadstrings = data.specs_all_leadstrings()
    for subsubele in subsubels:
        assert (
            subsubele in leadstrings
        ), f"Error: Sub-subelement {subsubele} in text(-) does not appear in the database."


def _populate_database():
    # Open the new database and check that specific data was inserted into the database.
    data = Data()
    data.set_database_file(TEST_DATABASE_FILE)

    (__, subsubels, q_ids, __) = cpp.ParsePool.collect_data_from_source()
    # Compare lead strings with the original database.
    leadstrings = data.specs_all_leadstrings()
    for subsubele in subsubels:
        assert (
            subsubele in leadstrings
        ), f"Error: Sub-subelement {subsubele} in text(-) does not appear in the database."

        # Compare question ids strings with the original database.
        question_ids = data.questions_leadstring(subsubele)
        for qid in list(q_ids[subsubele]):
            assert (
                qid in question_ids
            ), f"Error: Question {qid} in text(-) does not appear in the database."

    if os.path.exists(TEST_DATABASE_FILE):
        os.remove(TEST_DATABASE_FILE)
