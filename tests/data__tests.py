"""
Tests for the application module.
"""

import pytest
from sqlalchemy import exc

from examgenerator.constants import EXAMDB_FILE
from examgenerator.data import Data

# pylint: disable=invalid-name

valid_q_pairs = {
    1: 35,
    2: 35,
    3: 50,
}

valid_leadstring = {
    1: "T",
    2: "G",
    3: "E",
}


@pytest.mark.parametrize("pool, count", valid_q_pairs.items())
def test_pool_questions(pool: int, count: int):
    data = Data()
    data.set_database_file(EXAMDB_FILE)

    q = data.pool_questions(pool)
    assert q == count


@pytest.mark.parametrize("pool, leadstring", valid_leadstring.items())
def test_pool_leadstrings(pool: int, leadstring: str):
    data = Data()
    data.set_database_file(EXAMDB_FILE)

    q = data.specs_pool_leadstrings(pool)
    for i in q:
        assert i[0] == leadstring


# CAUTION: Because Data is a shared state class, the change this test
# makes affects tests that follow. To avoid this, the pre-test state
# is captured and restored following the test.
def test_no_dbfile():
    data = Data()
    old_database_file = data.engine_file
    data.set_database_file(None)

    with pytest.raises(exc.ArgumentError):
        __ = data.pools()

    data.set_database_file(old_database_file)
