"""
Tests for the output_exam module.
"""

import os
from tempfile import mkstemp
from unittest import mock

import pytest

from examgenerator import output_exam

# pylint: disable=invalid-name

valid_pairs = {
    1: "Technician",
    2: "General",
    3: "Extra",
}

rand_generators = ["ABCD", "DCBA"]
output_types = ["text.j2", "html.j2", "latex.j2"]

TEMP_PATH = "/tmp" if os.path.exists("/tmp") and os.path.isdir("tmp") else None
mock.patch.object(output_exam.Data, "engine_file", "ExamData.db")


@pytest.mark.parametrize("pool, pool_name", valid_pairs.items())
def test_pool_spec(output_test_data, pool: int, pool_name: str):
    fd, path = mkstemp(dir=TEMP_PATH)
    with open(path, "w", encoding="utf-8") as outfile:
        output_exam.output_exam_jinja2(
            outstream=outfile,
            pool=pool,
            questions=output_test_data["questions"],
            examnumber=output_test_data["examnumber"],
            generator=output_test_data["generator"],
            template=output_test_data["template"],
            jinja_file=output_test_data["jinja_file"],
        )
    with open(fd, "r") as f:
        captured: str = f.read()
    os.unlink(path)
    assert pool_name in captured


@pytest.mark.parametrize("pool", list(valid_pairs))
def test_text_output(output_test_data, pool):
    fd, path = mkstemp(dir=TEMP_PATH)
    with open(path, "w", encoding="utf-8") as outfile:
        output_exam.output_exam_jinja2(
            outstream=outfile,
            pool=pool,
            questions=output_test_data["questions"],
            examnumber=output_test_data["examnumber"],
            generator=output_test_data["generator"],
            template=output_test_data["template"],
            jinja_file="text.j2",
        )
    with open(fd, "r") as f:
        captured: str = f.read()
    os.unlink(path)
    assert "------------------------" in captured
    assert "Written Examination" in captured
    assert 'content="Amateur Radio Example Exams"' not in captured
    assert "begin{enumerate}" not in captured


@pytest.mark.parametrize("pool", list(valid_pairs))
def test_html_output(output_test_data, pool):
    fd, path = mkstemp(dir=TEMP_PATH)
    with open(path, "w", encoding="utf-8") as outfile:
        output_exam.output_exam_jinja2(
            outstream=outfile,
            pool=pool,
            questions=output_test_data["questions"],
            examnumber=output_test_data["examnumber"],
            generator=output_test_data["generator"],
            template=output_test_data["template"],
            jinja_file="html.j2",
        )
    with open(fd, "r") as f:
        captured: str = f.read()
    os.unlink(path)
    assert "------------------------" not in captured
    assert "<body>" in captured
    assert 'content="Amateur Radio Example Exams"' in captured
    assert "begin{enumerate}" not in captured


@pytest.mark.parametrize("pool", list(valid_pairs))
def test_latex_output(output_test_data, pool):
    fd, path = mkstemp(dir=TEMP_PATH)
    with open(path, "w", encoding="utf-8") as outfile:
        output_exam.output_exam_jinja2(
            outstream=outfile,
            pool=pool,
            questions=output_test_data["questions"],
            examnumber=output_test_data["examnumber"],
            generator=output_test_data["generator"],
            template=output_test_data["template"],
            jinja_file="latex.j2",
        )
    with open(fd, "r") as f:
        captured: str = f.read()
    os.unlink(path)
    assert "------------------------" not in captured
    assert "documentclass" in captured
    assert 'content="Amateur Radio Example Exams"' not in captured
    assert "begin{enumerate}" in captured


@pytest.mark.parametrize(
    "rand,o_type", [(x, y) for x in rand_generators for y in output_types],
)
def test_admonition_abcd_dcba(output_test_data, rand, o_type):
    for item in valid_pairs:
        if not isinstance(int(item), int):
            continue
        fd, path = mkstemp(dir=TEMP_PATH)
        with open(path, "w", encoding="utf-8") as outfile:
            output_exam.output_exam_jinja2(
                outstream=outfile,
                pool=item,
                questions=output_test_data["questions"],
                examnumber=output_test_data["examnumber"],
                generator=output_test_data["generator"],
                template=rand,
                jinja_file=o_type,
            )
        with open(fd, "r") as f:
            captured: str = f.read()
        os.unlink(path)
        assert "NOT A VALID AMATEUR RADIO EXAM" in captured
