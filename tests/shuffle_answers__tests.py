"""
Tests for the shuffle_answers module.
"""

from typing import List

import pytest

from examgenerator.shuffle_answers import (abcd_generator, dcba_generator,
                                           random_generator, shuffle_answer)

answer_enum = {
    0: ["Ans", "Dis_1", "Dis_2", "Dis_3"],
    1: ["Dis_1", "Ans", "Dis_2", "Dis_3"],
    2: ["Dis_1", "Dis_2", "Ans", "Dis_3"],
    3: ["Dis_1", "Dis_2", "Dis_3", "Ans"],
}
expected_abcd_list = [0, 1, 2, 3, 0, 1, 2, 3]
expected_dcba_list = [3, 2, 1, 0, 3, 2, 1, 0]
routine_list = {
    abcd_generator: expected_abcd_list,
    dcba_generator: expected_dcba_list,
}


def test_random_generator():
    """ Test the random generator. """

    maximum = 4
    gen = random_generator(maximum)
    for _ in range(maximum):
        test = next(gen)
        print(f"Generated: {test}")
        assert test >= 0
        assert test < maximum


@pytest.mark.parametrize("correct, value", answer_enum.items())
def test_random_shuffle_answer(correct: int, value: List[str]):
    """ Test the random shuffle routine. """

    routine = random_generator
    # Iterate over the possible answer and distractor lists
    print(f"Correct answer: {correct}")
    old_answer = value
    # Note: this resets the generator
    gen = routine(len(old_answer))
    # Verify the shuffled answer lists are the same size and contain the correct
    # answer in the correct slot.
    (new_answer, new_list) = shuffle_answer(list(old_answer), correct, gen)
    assert len(old_answer) == len(new_list)
    assert new_list[new_answer] == "Ans"


@pytest.mark.parametrize("routine, value", routine_list.items())
def test_generator_routines(routine, value: List[int]):
    """ Test the generator routines defined in routine_list above. """

    maximum = 4

    gen = routine(maximum)
    verify_list = value
    for item in verify_list:
        test = next(gen)
        print(f"Generated: {test}")
        assert test == item


@pytest.mark.parametrize("routine, value", routine_list.items())
def test_routine_list_shuffle_answer(routine, value: List[int]):
    """ Test the shuffle routines defined in routine_list above. """

    # Iterate over the routines defined above.

    verify_list = value
    print(f"Running against {verify_list}")
    # Iterate over the possible answer and distractor lists
    for correct, a_value in answer_enum.items():
        print(f"Correct answer: {correct}")
        old_answer = a_value
        # Note this resets the generator
        gen = routine(len(old_answer))
        # Verify the shuffled answer lists are the same size and contain the correct
        # answer in the correct slot.
        for ans in verify_list:
            (new_answer, new_list) = shuffle_answer(list(old_answer), correct, gen)
            assert len(old_answer) == len(new_list)
            assert new_answer == ans
            assert new_list[new_answer] == "Ans"
