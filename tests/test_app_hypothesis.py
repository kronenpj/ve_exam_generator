"""
Hypothesis tests for the application module.
"""
import os
from tempfile import mkstemp
from unittest import mock

import pytest
from hypothesis import Verbosity, assume, given, settings
from hypothesis.strategies import text

from examgenerator.app import application

from .app__tests import valid_pairs

pool_arg = ["-p", "--pool="]


@pytest.mark.parametrize("arg", pool_arg)
# @pytest.mark.skip(reason="no way of currently testing this")
@pytest.mark.hypothesis
# @given(a=text(min_size=1), b=text(min_size=1))
@settings(verbosity=Verbosity.debug)
@given(a=text(min_size=1, max_size=1))
# @given(a=text())
def test_arg_random(a, arg):
    # assume(a != "")
    # assume(a is not None)
    assume(a.lower()[0] not in valid_pairs.keys())

    file_handle, path = mkstemp()
    with mock.patch("sys.argv", [""] + [f"-f{path}"] + [f"{arg}{a}"]), mock.patch(
        "examgenerator.pool_choice._get_input", return_value=a
    ), pytest.raises(SystemExit) as pytest_wrapped_e:
        application()
    # print(f"Argument a: {a}")
    assert pytest_wrapped_e.type == SystemExit
    with open(file_handle, "r") as in_file:
        captured: str = in_file.read()
    os.unlink(path)
    assert "You chose to generate" not in captured


# @pytest.mark.skip(reason="no way of currently testing this")
@pytest.mark.hypothesis
@settings(verbosity=Verbosity.debug)
@given(a=text(min_size=1, max_size=1))
# @given(a=text())
def test_prompt_random(a):
    # assume(a != "")
    # assume(a is not None)
    assume(a.lower()[0] not in list(valid_pairs.keys()))

    file_handle, path = mkstemp()
    with mock.patch("sys.argv", [""] + [f"-f{path}"]), mock.patch(
        "examgenerator.pool_choice._get_input", return_value=a
    ), pytest.raises(SystemExit) as pytest_wrapped_e:
        application()
    assert pytest_wrapped_e.type == SystemExit
    with open(file_handle, "r") as in_file:
        captured: str = in_file.read()
    os.unlink(path)
    assert "You chose to generate" not in captured
